CREATE TABLE user_roles (
    id SERIAL PRIMARY KEY,
    user_id integer NOT NULL,
    role_id integer NOT NULL,
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT role_id_fk FOREIGN KEY (role_id)
        REFERENCES roles (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
);
