CREATE TYPE role_status as ENUM ('DELETED', 'DISABLED', 'DRAFT', 'ENABLED');

CREATE TABLE roles (
    id SERIAL PRIMARY KEY,
    slug VARCHAR NOT NULL,
    display_name VARCHAR NOT NULL,
    description TEXT NULL,
    status role_status NULL DEFAULT 'DRAFT',
    created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP NULL
);

SELECT diesel_manage_updated_at('roles');