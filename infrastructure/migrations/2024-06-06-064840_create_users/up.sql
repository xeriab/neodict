CREATE TYPE user_status as ENUM ('ACTIVE', 'BANNED', 'DELETED', 'INACTIVE', 'PENDING');

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    username VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    first_name VARCHAR NULL,
    middle_name VARCHAR NULL,
    last_name VARCHAR NULL,
    password VARCHAR NOT NULL,
    status user_status NULL DEFAULT 'PENDING',
--    is_admin SMALLINT NULL DEFAULT 0,
    is_admin BOOLEAN NULL DEFAULT false,
    login_session TEXT NULL,
    last_logged_at TIMESTAMP NULL,
    created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP NULL
);

SELECT diesel_manage_updated_at('users');