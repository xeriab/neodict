CREATE TABLE login_history
(
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INTEGER NOT NULL REFERENCES users(id),
    logged_in_at TIMESTAMP NOT NULL
);