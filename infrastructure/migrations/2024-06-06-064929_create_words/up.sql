CREATE TYPE word_status as ENUM ('APPROVED', 'DELETED', 'DRAFT', 'REJECTED');

CREATE TABLE words (
    id SERIAL PRIMARY KEY,
    word VARCHAR NOT NULL,
    character VARCHAR NOT NULL,
    definition TEXT NOT NULL,
    status word_status NULL DEFAULT 'DRAFT',
--    submitted_by INTEGER NULL,
--    approved_by INTEGER NULL,
--    CONSTRAINT submitted_by_fk FOREIGN KEY (submitted_by)
--        REFERENCES users (id) MATCH SIMPLE
--        ON UPDATE CASCADE
--        ON DELETE CASCADE,
--    CONSTRAINT approved_by_fk FOREIGN KEY (approved_by)
--        REFERENCES users (id) MATCH SIMPLE
--        ON UPDATE CASCADE
--        ON DELETE CASCADE,
    submitted_by INTEGER NULL REFERENCES users(id),
    approved_by INTEGER NULL REFERENCES users(id),
    approved_at TIMESTAMP NULL,
    created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP NULL
);

SELECT diesel_manage_updated_at('words');