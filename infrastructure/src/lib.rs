#![feature(proc_macro_hygiene, decl_macro)]
#![allow(proc_macro_derive_resolution_fallback)]

#[macro_use]
extern crate diesel;
// #[macro_use]
// extern crate diesel_migrations;
#[macro_use]
extern crate log;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;

use std::env;

use diesel::{pg::PgConnection, prelude::*};
use dotenvy::dotenv;

// TODO: Use the (R2D2) connection pool as not to create a connection everytime it needs to be used.
pub fn establish_connection() -> PgConnection {
    dotenv().ok();
    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set.");
    PgConnection::establish(&db_url).unwrap_or_else(|_| panic!("Error connecting to {}", db_url))
}

// embed_migrations!();

#[rocket_sync_db_pools::database("neodict")]
pub struct DB(PgConnection);
