use std::time::Duration;

use rocket::{catchers, Rocket, Route, routes};
use rocket::fairing::AdHoc;
use rocket::http::{Cookie, CookieJar, Method};
use serde::{Deserialize, Serialize};

use neodict_api::controllers::auth_controller::{signin, signup, logout};
use neodict_api::controllers::word_controller::{
    list_words,
    submit_word,
    list_word,
    get_random_word,
    list_by_char,
    search_word_by
};

#[derive(Default, Serialize, Deserialize)]
pub struct Data {
    pub message: String,
}

#[rocket::get("/")]
async fn api_v1(cookie: &CookieJar<'_>) -> rocket::serde::json::Json<Data> {
    dbg!(cookie);
    let data = Data { message: String::from("Hello, World!") };
    rocket::serde::json::Json::from(data)
}

#[rocket::catch(404)]
fn not_found(req: &rocket::Request) -> rocket::serde::json::Json<Data> {
    let err = format!("Route {} is not found", req.uri());
    rocket::serde::json::Json::from(Data { message: err })
}

#[rocket::main]
async fn main() -> Result<(), rocket::Error> {
    let _rocket = rocket::build()
        .mount("/api/v1", routes![api_v1])
        .mount("/", routes![signup, signin, logout])
        .mount("/", routes![search_word_by, list_words, list_word, submit_word, get_random_word, list_by_char])
        .register("/", catchers![not_found])
        .launch()
        .await?;

    Ok(())
}