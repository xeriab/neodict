use std::string::String;

use rocket::{request, Request};
use rocket::http::{Cookie, CookieJar, Status};
use rocket::request::FromRequest;
use rocket::response::status;
use rocket::serde::json::{json, Json};

use neodict_domain::constants::*;
use neodict_domain::jwt;
use neodict_domain::models::user::{LoginDTO, LoginInfoDTO, User, UserDTO};
use neodict_domain::prelude::{INVITE_CODE, RequestString};
use neodict_shared::models::{Response, ResponseWithStatus};

pub fn signup(user: UserDTO, invite_code: Result<RequestString, status::Custom<Json<Response>>>) -> ResponseWithStatus {
    let mut admin: Option<bool> = Some(false);

    if invite_code.is_ok() {
        let code = invite_code.unwrap();
        if code.0.as_str() == INVITE_CODE {
            admin = Some(true);
        }
    } else {
        admin = Some(false);
    }

    let mut data = user;

    data.is_admin = admin;

    if User::signup(data.clone()) {
        ResponseWithStatus {
            status_code: Status::Ok.code,
            response: Response {
                message: String::from(MESSAGE_SIGNUP_SUCCESS),
                data: serde_json::to_value(data.clone()).unwrap(),
            },
        }
    } else {
        ResponseWithStatus {
            status_code: Status::BadRequest.code,
            response: Response {
                message: String::from(MESSAGE_SIGNUP_FAILED),
                data: serde_json::to_value(()).unwrap(),
            },
        }
    }
}

pub fn signin(login: LoginDTO, jar: &CookieJar<'_>) -> ResponseWithStatus {
    if let Some(result) = User::login(login, jar) {
        let token = jwt::generate_token(result);
        let _ts = serde_json::to_value(json!({ "token": token, "type": "Bearer" })).unwrap().as_str();

        let cookie = Cookie::new("token", token.clone());
        jar.add(cookie);

        dbg!(jar);

        ResponseWithStatus {
            status_code: Status::Ok.code,
            response: Response {
                message: String::from(MESSAGE_LOGIN_SUCCESS),
                data: serde_json::to_value(json!({ "token": token, "type": "Bearer" }))
                    .unwrap(),
            },
        }
    } else {
        ResponseWithStatus {
            status_code: Status::BadRequest.code,
            response: Response {
                message: String::from(MESSAGE_LOGIN_FAILED),
                data: serde_json::to_value(()).unwrap(),
            },
        }
    }
}

pub fn logout(token: String) -> ResponseWithStatus {
    if User::logout(token) {
        ResponseWithStatus {
            status_code: Status::BadRequest.code,
            response: Response {
                message: String::from(MESSAGE_LOGIN_FAILED),
                data: serde_json::to_value(()).unwrap(),
            },
        }
    } else {
        ResponseWithStatus {
            status_code: Status::BadRequest.code,
            response: Response {
                message: String::from(MESSAGE_LOGIN_FAILED),
                data: serde_json::to_value(()).unwrap(),
            },
        }
    }
}