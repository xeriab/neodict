use neodict_domain::schema::words::word;
use rocket::{
    delete as DELETE,
    get as GET, post as POST, put as PUT, response::status::{self, Created, NotFound},
    serde::json::Json,
};
use rocket::http::Status;
use serde_json::to_string as to_json_string;

use neodict_app::handlers::word::create::create_word;
use neodict_app::handlers::word::read::{list_word as fetch_word, search_by};
use neodict_app::handlers::word::read::list_words as fetch_words;
use neodict_app::handlers::word::read::list_words_by_char as fetch_all_by_char;
use neodict_domain::constants::*;
use neodict_domain::jwt::UserToken;
use neodict_domain::models::{NewWord, User};
use neodict_domain::models::user::{LoginDTO, UserDTO};
use neodict_domain::models::word::WordDTO;
use neodict_shared::models::{Response, ResponseWithStatus};
use rand::seq::{IndexedRandom, SliceRandom};
use neodict_domain::prelude::RequestString;

///
/// Most implementations here are so damn naive, we can make use caching and apply more safety rules
/// if we are in production environment.
///

#[GET("/api/v1/words", format = "application/json")]
pub fn list_words() -> status::Custom<Json<Response>> {
    let res = ResponseWithStatus {
        status_code: Status::Ok.code,
        response: Response {
            message: String::from(MESSAGE_OK),
            data: serde_json::to_value(fetch_words()).unwrap(),
        },
    };

    status::Custom(
        Status::from_code(res.status_code).unwrap(),
        Json(res.response),
    )
}

#[GET("/api/v1/words/random", format = "application/json")]
pub fn get_random_word() -> status::Custom<Json<Response>> {
    let res = ResponseWithStatus {
        status_code: Status::Ok.code,
        response: Response {
            message: String::from(MESSAGE_OK),
            data: serde_json::to_value(fetch_words().choose(&mut rand::thread_rng())).unwrap(),
        },
    };

    status::Custom(
        Status::from_code(res.status_code).unwrap(),
        Json(res.response),
    )
}

#[GET("/api/v1/words/by/<ch>", format = "application/json")]
pub fn list_by_char(ch: &str) -> status::Custom<Json<Response>> {
    let res = ResponseWithStatus {
        status_code: Status::Ok.code,
        response: Response {
            message: String::from(MESSAGE_OK),
            data: serde_json::to_value(fetch_all_by_char(ch)).unwrap(),
        },
    };

    status::Custom(
        Status::from_code(res.status_code).unwrap(),
        Json(res.response),
    )
}

#[GET("/api/v1/search/<w>")]
pub fn search_word_by(w: &str) -> status::Custom<Json<Response>> {
    dbg!(w);
    let res = ResponseWithStatus {
        status_code: Status::Ok.code,
        response: Response {
            message: String::from(MESSAGE_OK),
            data: serde_json::to_value(search_by(w)).unwrap(),
        },
    };

    status::Custom(
        Status::from_code(res.status_code).unwrap(),
        Json(res.response),
    )
}

#[GET("/api/v1/words/<id>", format = "application/json")]
pub fn list_word(id: i32) -> status::Custom<Json<Response>> {
    let res = ResponseWithStatus {
        status_code: Status::Ok.code,
        response: Response {
            message: String::from(MESSAGE_OK),
            data: serde_json::to_value(fetch_word(id).unwrap()).unwrap(),
        },
    };

    status::Custom(
        Status::from_code(res.status_code).unwrap(),
        Json(res.response),
    )
}

#[POST("/api/v1/words/submit", format = "application/json", data = "<w>")]
pub fn submit_word(mut w: Json<NewWord>, token: Result<UserToken, status::Custom<Json<Response>>>) -> status::Custom<Json<Response>> {
    let mut id: Option<i32> = None;

    if token.is_ok() {
        let user = User::find_by_id(&token.unwrap().user_id);

        if let Some(user) = user {
            if user.is_admin.unwrap() {
                return status::Custom(
                    Status::from_code(401).unwrap(),
                    Json(Response { message: MESSAGE_INVALID_TOKEN.to_string(), data: serde_json::to_value(()).unwrap() }),
                );
            }

            id = Option::from(user.id);
        }
    }

    w.submitted_by = id;

    let res = create_word(w);

    return status::Custom(
        Status::from_code(res.status_code).unwrap(),
        Json(res.response),
    );
}