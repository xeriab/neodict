use rocket::{
    delete as DELETE, get as GET, post as POST, put as PUT,
    response::status::{self, Created, NotFound},
    serde::json::Json,
};
use rocket::http::{Cookie, Status};
use rocket::yansi::Paint;
use serde_json::{to_string as to_json_string, to_value};

use neodict_domain::models::User;
use neodict_domain::models::user::{LoginDTO, UserDTO};
use neodict_domain::prelude::RequestString;
use neodict_shared::models::Response;

use crate::services::auth_service;

pub struct AuthController;

#[POST("/api/v1/auth/signup", format = "application/json", data = "<user>")]
pub fn signup(user: Json<UserDTO>, invite_code: Result<RequestString, status::Custom<Json<Response>>>) -> status::Custom<Json<Response>> {
    let res = auth_service::signup(user.0, invite_code);
    status::Custom(
        Status::from_code(res.status_code).unwrap(),
        Json(res.response),
    )
}

#[POST("/api/v1/auth/signin", format = "application/json", data = "<login>")]
pub fn signin(login: Json<LoginDTO>, jar: &rocket::http::CookieJar<'_>) -> status::Custom<Json<Response>> {
    let res = auth_service::signin(login.0, jar);
    status::Custom(
        Status::from_code(res.status_code).unwrap(),
        Json(res.response),
    )
}

#[GET("/api/v1/auth/logout", format = "application/json")]
pub fn logout(jar: &rocket::http::CookieJar<'_>) -> status::Custom<Json<Response>> {
    jar.resetting();
    status::Custom(
        Status::from_code(200).unwrap(),
        Json(Response{message: "".into(), data: to_value(()).unwrap()}),
    )
}
