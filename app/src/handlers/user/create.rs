use diesel::{insert_into, prelude::*};
use rocket::{response::status::Created, serde::json::Json};
use serde_json::to_string as to_json_string;
use serde_json::to_value as to_json_value;

use neodict_domain::constants::*;
use neodict_domain::prelude::{NewUser, User};
use neodict_infrastructure::establish_connection;
use neodict_shared::models::Response;

pub fn create_user(user: Json<NewUser>) -> Created<String> {
    use neodict_domain::schema::users::table;

    let user = user.into_inner();

    match insert_into(table)
        .values(&user)
        .get_result::<User>(&mut establish_connection())
    {
        Ok(user) => {
            // let response = Response {
            //     body: ResponseBody::User(user),
            // };


            let response = Response {
                message: MESSAGE_CREATED.into(),
                data: to_json_value(user.clone()).unwrap(),
            };

            Created::new("").tagged_body(to_json_string(&response).unwrap())
        }
        // Doesn't seem like insert_into() will throw any errors, leaving room for specific error
        // handling just in case though
        Err(err) => match err {
            _ => {
                panic!("Database error - {}", err);
            }
        },
    }
}
