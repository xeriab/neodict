use diesel::prelude::*;
use rocket::response::status::NotFound;

use neodict_domain::models::User;
use neodict_infrastructure::establish_connection;
use neodict_shared::models::Response;

pub fn list_user(user_id: i32) -> Result<User, NotFound<String>> {
    use neodict_domain::schema::users;

    match users::table
        .find(user_id)
        .first::<User>(&mut establish_connection())
    {
        Ok(user_) => Ok(user_),
        Err(err) => match err {
            diesel::result::Error::NotFound => {
                let response = Response {
                    message: format!("Error selecting user with id {} - {}", user_id, err),
                    data: Default::default(),
                };
                return Err(NotFound(serde_json::to_string(&response).unwrap()));
            }
            _ => {
                panic!("Database error - {}", err);
            }
        },
    }
}

pub fn list_users() -> Vec<User> {
    use neodict_domain::schema::users;

    match users::table
        .select(users::all_columns)
        .load::<User>(&mut establish_connection())
    {
        Ok(mut users) => {
            users.sort();
            users
        }
        // doesn't seem like selecting everything will throw any errors, leaving room for specific
        // error handling just in case though
        Err(err) => match err {
            _ => {
                panic!("Database error - {}", err);
            }
        },
    }
}
