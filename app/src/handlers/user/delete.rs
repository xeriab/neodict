use diesel::prelude::*;
use rocket::response::status::NotFound;

use neodict_domain::prelude::User;
use neodict_infrastructure::establish_connection;
use neodict_shared::models::Response;

pub fn delete_user(user_id: i32) -> Result<Vec<User>, NotFound<String>> {
    use neodict_domain::schema::{users, users::dsl::*};

    let response: Response;

    let num_deleted =
        match diesel::delete(users.filter(id.eq(user_id))).execute(&mut establish_connection()) {
            Ok(count) => count,
            Err(err) => match err {
                diesel::result::Error::NotFound => {
                    let response = Response {
                        message: format!("Error publishing user with id {} - {}", user_id, err),
                        data: Default::default(),
                    };
                    return Err(NotFound(serde_json::to_string(&response).unwrap()));
                }
                _ => {
                    panic!("Database error - {}", err);
                }
            },
        };

    if num_deleted > 0 {
        match users::table
            .select(users::all_columns)
            .load::<User>(&mut establish_connection())
        {
            Ok(mut users_) => {
                users_.sort();
                Ok(users_)
            }
            // doesn't seem like selecting everything will throw any errors, leaving room for
            // specific error handling just in case though
            Err(err) => match err {
                _ => {
                    panic!("Database error - {}", err);
                }
            },
        }
    } else {
        response = Response {
            message: format!("Error - no user with id {}", user_id),
            data: Default::default(),
        };
        Err(NotFound(serde_json::to_string(&response).unwrap()))
    }
}
