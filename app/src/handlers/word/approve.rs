use diesel::prelude::*;
use rocket::response::status::NotFound;

use neodict_domain::enums::WordStatus;
use neodict_domain::models::Word;
use neodict_infrastructure::establish_connection;
use neodict_shared::models::Response;

pub fn approve_word(word_id: i32, by: i32) -> Result<Word, NotFound<String>> {
    use neodict_domain::schema::words::dsl::*;

    match diesel::update(words.find(word_id))
        .set(status.eq(WordStatus::Approved))
        // .set(approved_by.eq(by))
        .get_result::<Word>(&mut establish_connection()) {
        Ok(word_) => Ok(word_),
        Err(err) => match err {
            diesel::result::Error::NotFound => {
                let response = Response {
                    message: format!("Error approving word with id {} - {}", word_id, err),
                    data: Default::default(),
                };

                return Err(NotFound(serde_json::to_string(&response).unwrap()));
            }
            _ => {
                panic!("Database error - {}", err);
            }
        }
    }
}
