use diesel::prelude::*;
use rocket::response::status::NotFound;

use neodict_domain::prelude::Word;
use neodict_infrastructure::establish_connection;
use neodict_shared::models::Response;

pub fn delete_word(word_id: i32) -> Result<Vec<Word>, NotFound<String>> {
    use neodict_domain::schema::{words, words::dsl::*};

    let response: Response;

    let num_deleted =
        match diesel::delete(words.filter(id.eq(word_id))).execute(&mut establish_connection()) {
            Ok(count) => count,
            Err(err) => match err {
                diesel::result::Error::NotFound => {
                    let response = Response {
                        message: format!("Error publishing word with id {} - {}", word_id, err),
                        data: Default::default(),
                    };
                    return Err(NotFound(serde_json::to_string(&response).unwrap()));
                }
                _ => {
                    panic!("Database error - {}", err);
                }
            },
        };

    if num_deleted > 0 {
        match words::table
            .select(words::all_columns)
            .load::<Word>(&mut establish_connection())
        {
            Ok(mut words_) => {
                words_.sort();
                Ok(words_)
            }
            // doesn't seem like selecting everything will throw any errors, leaving room for
            // specific error handling just in case though
            Err(err) => match err {
                _ => {
                    panic!("Database error - {}", err);
                }
            },
        }
    } else {
        response = Response {
            message: format!("Error - no word with id {}", word_id),
            data: Default::default(),
        };
        Err(NotFound(serde_json::to_string(&response).unwrap()))
    }
}
