use diesel::{insert_into, prelude::*};
use rocket::http::Status;
use rocket::serde::json::Json;
use serde_json::to_value as to_json_value;

use neodict_domain::constants::MESSAGE_OK;
use neodict_domain::prelude::{NewWord, Word};
use neodict_infrastructure::establish_connection;
use neodict_shared::models::{Response, ResponseWithStatus};

pub fn create_word(word: Json<NewWord>) -> ResponseWithStatus {
    use neodict_domain::schema::words::table;

    let word = word.into_inner();

    match insert_into(table)
        .values(&word)
        .get_result::<Word>(&mut establish_connection())
    {
        Ok(word) => {
            ResponseWithStatus {
                status_code: Status::Ok.code,
                response: Response {
                    message: MESSAGE_OK.to_string(),
                    data: to_json_value(word).unwrap(),
                },
            }
        }
        // Doesn't seem like insert_into() will throw any errors, leaving room for specific error
        // handling just in case though
        Err(err) => match err {
            _ => {
                panic!("Database error - {}", err);
            }
        },
    }
}
