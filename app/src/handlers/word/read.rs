use diesel::internal::derives::multiconnection::SelectStatementAccessor;
use diesel::prelude::*;
use rocket::response::status::NotFound;

use neodict_domain::models::word::{WordDTO, Word};
use neodict_domain::prelude::RequestString;
use neodict_domain::schema::words::dsl::words;
use neodict_infrastructure::establish_connection;
use neodict_shared::models::Response;

pub fn list_word(word_id: i32) -> Result<Word, NotFound<String>> {
    use neodict_domain::schema::words;

    match words::table
        .find(word_id)
        .first::<Word>(&mut establish_connection())
    {
        Ok(word_) => Ok(word_),
        Err(err) => match err {
            diesel::result::Error::NotFound => {
                let response = Response {
                    message: format!(
                        "Error selecting word with id {} - {}",
                        word_id, err
                    ),
                    data: Default::default(),
                };
                return Err(NotFound(serde_json::to_string(&response).unwrap()));
            }
            _ => {
                panic!("Database error - {}", err);
            }
        },
    }
}

pub fn list_words_by_char(ch: &str) -> Vec<Word> {
    use neodict_domain::schema::words;

    dbg!(ch.clone());

    match words::table
        .select(words::all_columns)
        .filter(words::character.eq(ch))
        .get_results::<Word>(&mut establish_connection())
    {
        Ok(mut words_) => {
            words_.sort();
            words_
        }
        // doesn't seem like selecting everything will throw any errors, leaving room for specific
        // error handling just in case though
        Err(err) => match err {
            _ => {
                panic!("Database error - {}", err);
            }
        },
    }
}

pub fn search_by(word: &str) -> Vec<Word> {
    use neodict_domain::schema::words;

    match words::table
        .select(words::all_columns)
        .filter(words::word.like(format!("{}", word)))
        .get_results::<Word>(&mut establish_connection())
    {
        Ok(mut words_) => {
            words_.sort();
            words_
        }
        // doesn't seem like selecting everything will throw any errors, leaving room for specific
        // error handling just in case though
        Err(err) => match err {
            _ => {
                panic!("Database error - {}", err);
            }
        },
    }
}

pub fn list_words() -> Vec<Word> {
    use neodict_domain::schema::words;

    match words::table
        .select(words::all_columns)
        .load::<Word>(&mut establish_connection())
    {
        Ok(mut words_) => {
            words_.sort();
            words_
        }
        // doesn't seem like selecting everything will throw any errors, leaving room for specific
        // error handling just in case though
        Err(err) => match err {
            _ => {
                panic!("Database error - {}", err);
            }
        },
    }
}
