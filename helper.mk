# -*- tab-width: 2; encoding: utf-8; mode: makefile; -*-
#
# Copyright (c) 2024 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

#
# helper.mk
#

# vim: set ts=2 sw=2 tw=80 noet :
