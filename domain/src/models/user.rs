use std::time::SystemTime;

use bcrypt::{DEFAULT_COST, hash, verify};
use diesel::prelude::*;
use rocket::http::{Cookie, CookieJar};
use rocket::outcome::IntoOutcome;
use rocket::request::FromRequest;
use rocket::response::status;
use rocket::serde::{Deserialize, Serialize};
use rocket::serde::json::Json;
use uuid::Uuid;

use neodict_infrastructure::establish_connection;
use neodict_shared::models::model::Model;
use neodict_shared::models::Response;

use crate::enums::UserStatus;
use crate::jwt::UserToken;
use crate::models::login_history::LoginHistory;
use crate::prelude::{INVITE_CODE, RequestString};
use crate::schema::users;

/// **User** Model.
#[derive(Queryable, Selectable, Serialize, PartialEq, Clone, Debug, Ord, PartialOrd, Eq)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = users)]
pub struct User {
    /// User's ID.
    pub id: i32,

    /// User's name.
    pub username: String,

    /// User's email address.
    pub email: String,

    /// User's first name.
    pub first_name: Option<String>,

    /// User's middle name.
    pub middle_name: Option<String>,

    /// User's last name.
    pub last_name: Option<String>,

    /// User's password.
    pub password: String,

    /// User's status.
    pub status: Option<UserStatus>,

    pub is_admin: Option<bool>,

    /// User's login session.
    pub login_session: Option<String>,

    /// User's last logged in time.
    pub last_logged_at: Option<SystemTime>,

    /// Model's creation time.
    pub created_at: Option<SystemTime>,

    /// Model's update time.
    pub updated_at: Option<SystemTime>,

    /// Model's deletion time.
    pub deleted_at: Option<SystemTime>,
}

impl Model for User {}

/// **NewUser** `DTO`.
#[derive(Insertable, Deserialize)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = users)]
// #[diesel(check_for_backend(Pg))]
pub struct NewUser<'a> {
    /// User's name.
    pub username: &'a str,

    /// User's email address.
    pub email: &'a str,

    /// User's first name.
    pub first_name: Option<&'a str>,

    /// User's middle name.
    pub middle_name: Option<&'a str>,

    /// User's last name.
    pub last_name: Option<&'a str>,

    /// User's password.
    pub password: &'a str,

    /// User's status.
    pub status: Option<UserStatus>,

    pub is_admin: Option<bool>,

    /// User's login session.
    pub login_session: Option<&'a str>,

    /// User's last logged in time.
    pub last_logged_at: Option<SystemTime>,

    /// Model's creation time.
    pub created_at: Option<SystemTime>,

    /// Model's update time.
    pub updated_at: Option<SystemTime>,

    /// Model's deletion time.
    pub deleted_at: Option<SystemTime>,
}

/// **User** `DTO`.
#[derive(Insertable, Deserialize, Serialize, Debug, Clone)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = users)]
pub struct UserDTO {
    pub username: String,
    pub email: String,
    pub password: String,
    pub is_admin: Option<bool>,
}

// impl Model for UserDTO {}

/// **Login** `DTO`.
#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct LoginDTO {
    pub username_or_email: String,
    pub password: String,
}

// impl Model for LoginDTO {}

/// **LoginInfo** `DTO`.
#[derive(Insertable, Deserialize)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = users)]
#[diesel(belongs_to(crate::models::user::User, foreign_key = submitted_by))]
pub struct LoginInfoDTO {
    pub id: i32,
    pub username: String,
    pub login_session: String,
}

// impl Model for LoginInfoDTO {}

impl User {
    pub fn signup(user: UserDTO,) -> bool {
        let hashed_pwd = hash(&user.password, DEFAULT_COST).unwrap();

        let user = UserDTO {
            password: hashed_pwd,
            ..user
        };

        diesel::insert_into(users::table)
            .values(&user)
            .execute(&mut establish_connection())
            .is_ok()
    }

    pub fn login(login: LoginDTO, jar: &CookieJar<'_>) -> Option<LoginInfoDTO> {
        let user_to_verify = users::table
            .filter(users::username.eq(&login.username_or_email))
            .or_filter(users::email.eq(&login.username_or_email))
            .get_result::<User>(&mut establish_connection())
            .unwrap();

        if !user_to_verify.password.is_empty()
            && verify(&login.password, &user_to_verify.password).unwrap()
        {
            if let Some(login_history) = LoginHistory::create(&user_to_verify.username) {
                if !LoginHistory::save_login_history(login_history) {
                    return None;
                }

                let login_session_str = User::generate_login_session();

                jar.add(Cookie::new("user_id", user_to_verify.id.to_string()));

                User::update_login_session_to_db(&user_to_verify.username, &login_session_str);

                Some(LoginInfoDTO {
                    id: user_to_verify.id,
                    username: user_to_verify.username,
                    login_session: login_session_str,
                })
            } else {
                None
            }
        } else {
            None
        }
    }

    pub fn logout(token: String) -> bool {
        dbg!(token);
        false
    }

    pub fn is_valid_login_session(token: &UserToken) -> bool {
        users::table
            .filter(users::username.eq(&token.user))
            .filter(users::login_session.eq(&token.login_session))
            .get_result::<User>(&mut establish_connection())
            .is_ok()
    }

    pub fn find_by_username(username: &str) -> Option<User> {
        let result_user = users::table.filter(users::username.eq(username)).get_result::<User>(&mut establish_connection());
        if let Ok(user) = result_user {
            Some(user)
        } else {
            None
        }
    }

    pub fn find_by_id(id: &i32) -> Option<User> {
        let result_user = users::table.filter(users::id.eq(id)).get_result::<User>(&mut establish_connection());
        if let Ok(user) = result_user {
            Some(user)
        } else {
            None
        }
    }

    pub fn generate_login_session() -> String {
        Uuid::new_v4().simple().to_string()
    }

    pub fn update_login_session_to_db(username: &str, session: &str) -> bool {
        if let Some(user) = User::find_by_username(username) {
            diesel::update(users::table.find(user.id))
                .set(users::login_session.eq(session.to_string()))
                .execute(&mut establish_connection())
                .is_ok()
        } else {
            false
        }
    }
}

// #[rocket::async_trait]
// impl<'r> FromRequest<'r> for User {
//     type Error = std::convert::Infallible;
//
//     async fn from_request(request: &'r rocket::Request<'_>) -> rocket::request::Outcome<Self, Self::Error> {
//         request.cookies()
//             .get("user_id")
//             // .and_then(|c| c.value().parse().ok())
//             .and_then(|c| c.value().parse().ok())
//             .map(|id| User::find_by_username(id).unwrap())
//             // .map(|id| User::find_by_id(id).unwrap())
//             .or_forward(rocket::http::Status::Unauthorized)
//     }
// }