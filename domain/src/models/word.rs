use std::time::SystemTime;
use diesel::associations::BelongsTo;

use diesel::prelude::*;
use rocket::serde::{Deserialize, Serialize};
// use neodict_shared::models::model::Model;

use crate::enums::WordStatus;
// use crate::models::User;

/// **Word** Model.
#[derive(Queryable, Selectable, Serialize, Associations, PartialEq, Debug, Ord, PartialOrd, Eq)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = crate::schema::words)]
#[diesel(belongs_to(crate::models::user::User, foreign_key = submitted_by))]
pub struct Word {
    /// Word's ID.
    pub id: i32,

    /// Word.
    pub word: String,

    /// Word's definition.
    pub definition: String,

    /// Word's first character (As mentioned in the challenge).
    pub character: String,

    /// Word's status.
    pub status: Option<WordStatus>,

    /// Word's submitting user ID.
    pub submitted_by: Option<i32>,

    /// Word's approving user ID.
    pub approved_by: Option<i32>,

    /// Word's approving time.
    pub approved_at: Option<SystemTime>,

    /// Model's creation time.
    pub created_at: Option<SystemTime>,

    /// Model's update time.
    pub updated_at: Option<SystemTime>,

    /// Model's deletion time.
    pub deleted_at: Option<SystemTime>,
}

// impl BelongsTo<User> for Word {
//     type ForeignKey = crate::schema::words::submitted_by;
//
//     type ForeignKeyColumn = ();
//
//     fn foreign_key(&self) -> Option<&Self::ForeignKey> {
//         todo!()
//     }
//
//     fn foreign_key_column() -> Self::ForeignKeyColumn {
//         todo!()
//     }
// }

// impl Model for Word {}

/// **Word** DTO.
#[derive(Insertable, Deserialize)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = crate::schema::words)]
pub struct NewWord<'a> {
    /// Word.
    pub word: &'a str,

    /// Word's definition.
    pub definition: &'a str,

    /// Word's first character (As mentioned in the challenge).
    pub character: &'a str,

    /// Word's status.
    pub status: Option<WordStatus>,

    /// Word's submitting user ID.
    pub submitted_by: Option<i32>,

    /// Word's approving user ID.
    pub approved_by: Option<i32>,

    /// Word's approving time.
    pub approved_at: Option<SystemTime>,

    /// Model's creation time.
    pub created_at: Option<SystemTime>,

    /// Model's update time.
    pub updated_at: Option<SystemTime>,

    /// Model's deletion time.
    pub deleted_at: Option<SystemTime>,
}

/// **Word** DTO.
#[derive(Insertable, Deserialize)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = crate::schema::words)]
pub struct WordDTO<'a> {
    /// Word.
    pub word: &'a str,

    /// Word's definition.
    pub definition: &'a str,

    /// Word's first character (As mentioned in the challenge).
    pub character: &'a str,

    /// Word's submitting user ID.
    pub submitted_by: Option<i32>,
}
