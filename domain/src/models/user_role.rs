use std::cmp::{Eq, Ord, PartialEq, PartialOrd};

use diesel::prelude::*;
use rocket::serde::{Deserialize, Serialize};

#[derive(Queryable, Selectable, Serialize, PartialEq, Debug, Ord, PartialOrd, Eq)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = crate::schema::user_roles)]
#[diesel(belongs_to(crate::models::user::User, foreign_key = user_id))]
#[diesel(belongs_to(crate::models::role::Role, foreign_key = role_id))]
pub struct UserRole {
    /// User's ID.
    pub id: i32,

    /// Related user ID.
    pub user_id: i32,

    // Related role ID.
    pub role_id: i32,
}

#[derive(Insertable, Deserialize)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = crate::schema::user_roles)]
pub struct NewUserRole {
    /// Related user ID.
    pub user_id: i32,

    /// Related role ID.
    pub role_id: i32,
}
