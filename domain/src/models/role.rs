use std::time::SystemTime;

use diesel::prelude::*;
use rocket::serde::{Deserialize, Serialize};

use crate::enums::RoleStatus;

#[derive(Queryable, Selectable, Serialize, PartialEq, Debug, Ord, PartialOrd, Eq)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = crate::schema::roles)]
pub struct Role {
    /// Role's ID.
    pub id: i32,

    /// Role's slug.
    pub slug: String,

    /// Role's display name.
    pub display_name: String,

    /// Role's description.
    pub description: Option<String>,

    /// User's status.
    pub status: Option<RoleStatus>,

    /// Model's creation time.
    pub created_at: Option<SystemTime>,

    /// Model's update time.
    pub updated_at: Option<SystemTime>,

    /// Model's deletion time.
    pub deleted_at: Option<SystemTime>,
}

#[derive(Insertable, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = crate::schema::roles)]
pub struct NewRole<'a> {
    /// Role's slug.
    pub slug: &'a str,

    /// Role's display name.
    pub display_name: &'a str,

    /// Role's description.
    pub description: Option<&'a str>,
}
