// use chrono::{DateTime, Utc};
use diesel::prelude::*;
use rocket::serde::{Deserialize, Serialize};
use std::time::SystemTime;

use neodict_infrastructure::establish_connection;

use crate::models::User;

/// **LoginHistory** Model.
#[derive(Queryable, Identifiable, Selectable, Serialize, PartialEq, Debug, Ord, PartialOrd, Eq)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = crate::schema::login_history)]
pub struct LoginHistory {
    /// Model's ID.
    pub id: i32,

    /// User's ID.
    pub user_id: i32,

    /// Logged in time.
    pub logged_in_at: SystemTime,
}

#[derive(Insertable, Deserialize)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = crate::schema::login_history)]
pub struct LoginHistoryDTO {
    pub user_id: i32,
    pub logged_in_at: SystemTime,
}

impl LoginHistory {
    pub fn create(un: &str) -> Option<LoginHistoryDTO> {
        if let Some(user) = User::find_by_username(un) {
            Some(LoginHistoryDTO {
                user_id: user.id,
                logged_in_at: SystemTime::now(),
            })
        } else {
            None
        }
    }

    pub fn save_login_history(insert_record: LoginHistoryDTO) -> bool {
        use crate::schema::login_history::table;

        diesel::insert_into(table)
            .values(&insert_record)
            .execute(&mut establish_connection())
            .is_ok()
    }
}