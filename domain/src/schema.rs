// @generated automatically by Diesel CLI.

pub mod sql_types {
    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "role_status"))]
    pub struct RoleStatus;

    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "user_status"))]
    pub struct UserStatus;

    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "word_status"))]
    pub struct WordStatus;
}

diesel::table! {
    login_history (id) {
        id -> Int4,
        user_id -> Int4,
        logged_in_at -> Timestamp,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::RoleStatus;

    roles (id) {
        id -> Int4,
        slug -> Varchar,
        display_name -> Varchar,
        description -> Nullable<Text>,
        status -> Nullable<RoleStatus>,
        created_at -> Nullable<Timestamp>,
        updated_at -> Nullable<Timestamp>,
        deleted_at -> Nullable<Timestamp>,
    }
}

diesel::table! {
    user_roles (id) {
        id -> Int4,
        user_id -> Int4,
        role_id -> Int4,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::UserStatus;

    users (id) {
        id -> Int4,
        username -> Varchar,
        email -> Varchar,
        first_name -> Nullable<Varchar>,
        middle_name -> Nullable<Varchar>,
        last_name -> Nullable<Varchar>,
        password -> Varchar,
        status -> Nullable<UserStatus>,
        is_admin -> Nullable<Bool>,
        login_session -> Nullable<Text>,
        last_logged_at -> Nullable<Timestamp>,
        created_at -> Nullable<Timestamp>,
        updated_at -> Nullable<Timestamp>,
        deleted_at -> Nullable<Timestamp>,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::WordStatus;

    words (id) {
        id -> Int4,
        word -> Varchar,
        definition -> Text,
        character -> Varchar,
        status -> Nullable<WordStatus>,
        submitted_by -> Nullable<Int4>,
        approved_by -> Nullable<Int4>,
        approved_at -> Nullable<Timestamp>,
        created_at -> Nullable<Timestamp>,
        updated_at -> Nullable<Timestamp>,
        deleted_at -> Nullable<Timestamp>,
    }
}

diesel::joinable!(login_history -> users (user_id));
diesel::joinable!(user_roles -> roles (role_id));
diesel::joinable!(user_roles -> users (user_id));

diesel::allow_tables_to_appear_in_same_query!(
    login_history,
    roles,
    user_roles,
    users,
    words,
);
