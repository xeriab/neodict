//region Enums
//

use rocket::http::Status;
use rocket::Request;
use rocket::request::{FromRequest, Outcome};
use rocket::response::status;
use rocket::serde::json::{Json, serde_json};
use serde::{Deserialize, Serialize};

use neodict_shared::models::Response;

pub use crate::{enums as Enums, enums::*};
pub use crate::{models as Models, models::*};
use crate::constants::MESSAGE_UNKNOWN_ERROR;
// pub use crate::schema::*;
pub use crate::schema as Schema;

//
//endregion Enums

//region Models
//

//
//endregion Models

//region Schema
//

//
//endregion Schema

pub const INVITE_CODE: &str = "DummyInviteCode";

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct RequestString(pub String);

#[rocket::async_trait]
impl<'r> FromRequest<'r> for RequestString {
    type Error = status::Custom<Json<Response>>;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, status::Custom<Json<Response>>> {
        if let Some(invite_hdr) = req.headers().get_one("InviteCode") {
            let invite_code = invite_hdr.to_string();

            dbg!(invite_code.clone());

            return Outcome::Success(RequestString(invite_code));
        }

        let data = Json(Response {
            message: String::from(MESSAGE_UNKNOWN_ERROR),
            data: serde_json::to_value(RequestString("".to_string())).unwrap(),
        });

        Outcome::Error((
            Status::BadRequest,
            status::Custom(
                Status::Unauthorized,
                data,
            ),
        ))
    }
}

