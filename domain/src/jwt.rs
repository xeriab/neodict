use std::env;

use chrono::Utc;
use jsonwebtoken::{Header, Validation};
use jsonwebtoken::{DecodingKey, EncodingKey};
use jsonwebtoken::errors::Error;
use jsonwebtoken::errors::Result;
use jsonwebtoken::TokenData;
use rocket::http::Status;
use rocket::request::{FromRequest, Outcome, Request};
use rocket::response::status;
use rocket::serde::{Deserialize, Serialize};
use rocket::serde::json::{Json, serde_json};
use rocket::yansi::Paint;

use neodict_shared::models::Response;

// use models::response::Response;
use crate::models::user::{LoginInfoDTO, User};

static ONE_WEEK: usize = 60 * 60 * 24 * 7; // In seconds.


/// Naive `JWT` implementation
///
/// _**NOTE**: We can use more data fields to describe the token and also use more secure algorithms,
/// in other words the longer and random the token the better!_
///
/// _**NOTE**: I prefer to use `OAuth` version 2 for securing APIs because it allows us to add and
/// implement wide range of solutions such as scopes which makes dealing with APIs a bit easy._
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserToken {
    /// User's data.
    pub user: String,

    /// User's ID.
    pub user_id: i32,

    // User role.
    pub role: String,

    /// Session.
    pub login_session: String,

    /// Issued at.
    pub iat: usize,

    /// Expiration.
    pub exp: usize,

    pub sub: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Token {
    pub claims: UserToken,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for UserToken {
    type Error = status::Custom<Json<Response>>;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, status::Custom<Json<Response>>> {
        fn is_valid(key: &str) -> Result<UserToken> {
            Ok(decode_jwt(String::from(key))?)
        }

        match req.headers().get_one("Authorization") {
            None => {
                let response = Response { message: "Error validating JWT token - No token provided".into(), data: serde_json::to_value(()).unwrap() };
                // Outcome::Failure((Status::Unauthorized, response))
                Outcome::Error(
                    (
                        Status::BadRequest,
                        status::Custom(
                            Status::Unauthorized,
                            Json(response),
                        )
                    )
                )
            }
            Some(key) => match is_valid(key) {
                Ok(claims) => Outcome::Success(claims.clone()),
                Err(err) => match &err.kind() {
                    jsonwebtoken::errors::ErrorKind::ExpiredSignature => {
                        let response = Response { message: "Error validating JWT token - Expired Token".into(), data: serde_json::to_value(()).unwrap() };
                        // Outcome::Error((Status::Unauthorized, response))
                        Outcome::Error(
                            (
                                Status::BadRequest,
                                status::Custom(
                                    Status::Unauthorized,
                                    Json(response),
                                )
                            )
                        )
                    }
                    jsonwebtoken::errors::ErrorKind::InvalidToken => {
                        let response = Response { message: "Error validating JWT token - Invalid Token".into(), data: serde_json::to_value(()).unwrap() };
                        // Outcome::Error((Status::Unauthorized, response))
                        Outcome::Error(
                            (
                                Status::BadRequest,
                                status::Custom(
                                    Status::Unauthorized,
                                    Json(response),
                                )
                            )
                        )
                    }
                    _ => {
                        let response = Response { message: format!("Error validating JWT token - {}", err), data: serde_json::to_value(()).unwrap() };
                        Outcome::Error(
                            (
                                Status::BadRequest,
                                status::Custom(
                                    Status::Unauthorized,
                                    Json(response),
                                )
                            )
                        )
                    }
                }
            },
        }
    }
}

pub fn generate_token(login: LoginInfoDTO) -> String {
    let now = (Utc::now().timestamp_nanos_opt().unwrap() / 1_000_000_000) as usize; // nanosecond -> second
    let secret = env::var("JWT_SECRET").expect("JWT_SECRET must be set.");

    let payload = UserToken {
        iat: now,
        exp: now + ONE_WEEK,
        user: login.username,
        user_id: login.id,
        role: "".to_string(),
        login_session: login.login_session,
        sub: "".to_string(),
    };

    let headers = &Header::new(jsonwebtoken::Algorithm::HS512);
    let key = &EncodingKey::from_secret(secret.as_bytes());

    jsonwebtoken::encode(headers, &payload, key).unwrap()
}

fn decode_jwt(token: String) -> Result<UserToken> {
    let secret = env::var("JWT_SECRET").expect("JWT_SECRET must be set.");
    let token = token.trim_start_matches("Bearer").trim();

    match jsonwebtoken::decode::<UserToken>(
        &token,
        &DecodingKey::from_secret(secret.as_bytes()),
        &Validation::new(jsonwebtoken::Algorithm::HS512),
    ) {
        Ok(token) => Ok(token.claims),
        Err(err) => Err(Error::from(err.kind().to_owned()))
    }
}

fn decode_token(token: String) -> Result<TokenData<UserToken>> {
    jsonwebtoken::decode::<UserToken>(&token, &DecodingKey::from_secret(include_bytes!("secret.key")), &Validation::default())
}

fn verify_token(token_data: &TokenData<UserToken>) -> bool {
    User::is_valid_login_session(&token_data.claims)
}
