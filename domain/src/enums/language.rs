use rocket::serde::{Deserialize, Serialize};

/// **Language**.
#[derive(Debug, Default, PartialOrd, PartialEq, Eq, strum::Display, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub enum Language {
    /// Arabic language.
    #[strum(to_string = "ARABIC")]
    Arabic,

    /// English language.
    #[default]
    #[strum(to_string = "ENGLISH")]
    English,
}

// impl core::fmt::Display for Language {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         let rv = match self {
//             Language::Arabic => String::from("Arabic"),
//             Language::English => String::from("English"),
//             _ => String::from("Unknown"),
//         };
//
//         write!(f, "{}", rv)
//     }
// }

// impl TryFrom<&str> for Language {
//     type Error = ();
//
//     fn try_from(value: &str) -> Result<Self, Self::Error> {
//         match value {
//             "ar" => Ok(Language::Arabic),
//             "ar-SA" => Ok(Language::Arabic),
//             "ar-YE" => Ok(Language::Arabic),
//             "ar_SA" => Ok(Language::Arabic),
//             "ar_YE" => Ok(Language::Arabic),
//             "en" => Ok(Language::English),
//             "en-GB" => Ok(Language::English),
//             "en-US" => Ok(Language::English),
//             "en_GB" => Ok(Language::English),
//             "en_US" => Ok(Language::English),
//             _ => Ok(Language::English)
//         }
//     }
// }
//
// impl TryFrom<&String> for Language {
//     type Error = ();
//
//     fn try_from(value: &String) -> Result<Self, Self::Error> {
//         match value.as_str() {
//             "ar" => Ok(Language::Arabic),
//             "ar-SA" => Ok(Language::Arabic),
//             "ar-YE" => Ok(Language::Arabic),
//             "ar_SA" => Ok(Language::Arabic),
//             "ar_YE" => Ok(Language::Arabic),
//             "en" => Ok(Language::English),
//             "en-GB" => Ok(Language::English),
//             "en-US" => Ok(Language::English),
//             "en_GB" => Ok(Language::English),
//             "en_US" => Ok(Language::English),
//             _ => Ok(Language::English)
//         }
//     }
// }

impl From<&str> for Language {
    fn from(value: &str) -> Self {
        match value {
            "ar" => Language::Arabic,
            "ar-SA" => Language::Arabic,
            "ar-YE" => Language::Arabic,
            "ar_SA" => Language::Arabic,
            "ar_YE" => Language::Arabic,
            "en" => Language::English,
            "en-GB" => Language::English,
            "en-US" => Language::English,
            "en_GB" => Language::English,
            "en_US" => Language::English,
            _ => Language::English,
        }
    }
}

impl From<&String> for Language {
    fn from(value: &String) -> Self {
        match value.as_str() {
            "ar" => Language::Arabic,
            "ar-SA" => Language::Arabic,
            "ar-YE" => Language::Arabic,
            "ar_SA" => Language::Arabic,
            "ar_YE" => Language::Arabic,
            "en" => Language::English,
            "en-GB" => Language::English,
            "en-US" => Language::English,
            "en_GB" => Language::English,
            "en_US" => Language::English,
            _ => Language::English,
        }
    }
}

impl Language {
    /// Convert the current [`Language`] into locale.
    pub fn to_locale(&self) -> &str {
        match self {
            Self::Arabic => "ar",
            Self::English => "en",
        }
    }
}
