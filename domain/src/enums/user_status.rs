use std::io::Write;

use diesel::{
    deserialize::{self, FromSql, FromSqlRow},
    expression::AsExpression,
    pg::{Pg, PgValue},
    serialize::{self, IsNull, Output, ToSql},
};
use rocket::serde::{Deserialize, Serialize};

/// **User** Status.
#[derive(
    AsExpression,
    Clone,
    Debug,
    Default,
    Deserialize,
    Eq,
    FromSqlRow,
    Ord,
    PartialEq,
    PartialOrd,
    Serialize,
    strum::Display,
)]
#[serde(crate = "rocket::serde")]
#[diesel(sql_type = crate::schema::sql_types::UserStatus)]
pub enum UserStatus {
    ///
    #[strum(to_string = "ACTIVE")]
    Active,

    ///
    #[strum(to_string = "BANNED")]
    Banned,

    /// Deleted.
    #[strum(to_string = "DELETED")]
    Deleted,

    ///
    #[strum(to_string = "INACTIVE")]
    Inactive,

    ///
    #[strum(to_string = "PENDING")]
    #[default]
    Pending,
}

// impl core::fmt::Display for UserStatus {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         let rv = match self {
//             UserStatus::Active => String::from("Active"),
//             UserStatus::Banned => String::from("Banned"),
//             UserStatus::Deleted => String::from("Deleted"),
//             UserStatus::Inactive => String::from("Inactive"),
//             UserStatus::Pending => String::from("Pending"),
//             _ => String::from("Unknown"),
//         };
//
//         write!(f, "{}", rv)
//     }
// }

impl ToSql<crate::schema::sql_types::UserStatus, Pg> for UserStatus {
    fn to_sql<'b>(&'b self, out: &mut Output<'b, '_, Pg>) -> serialize::Result {
        match *self {
            UserStatus::Active => out.write_all(b"ACTIVE")?,
            UserStatus::Banned => out.write_all(b"BANNED")?,
            UserStatus::Deleted => out.write_all(b"DELETED")?,
            UserStatus::Inactive => out.write_all(b"INACTIVE")?,
            UserStatus::Pending => out.write_all(b"PENDING")?,
        }
        Ok(IsNull::No)
    }
}

impl FromSql<crate::schema::sql_types::UserStatus, Pg> for UserStatus {
    fn from_sql(bytes: PgValue) -> deserialize::Result<Self> {
        match bytes.as_bytes() {
            b"ACTIVE" => Ok(UserStatus::Active),
            b"BANNED" => Ok(UserStatus::Banned),
            b"DELETED" => Ok(UserStatus::Deleted),
            b"INACTIVE" => Ok(UserStatus::Inactive),
            b"PENDING" => Ok(UserStatus::Pending),
            _ => Err("Unrecognized enum variant".into()),
        }
    }
}
