use rocket::serde::{Deserialize, Serialize};

/// **Roles**.
#[derive(Debug, Default, PartialOrd, PartialEq, Eq, strum::Display, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub enum Roles {
    /// Admin.
    #[strum(to_string = "Admin")]
    Admin,

    /// Anonymous.
    #[strum(to_string = "Anonymous")]
    Anonymous,

    /// Moderator.
    #[strum(to_string = "Moderator")]
    Moderator,

    /// User.
    #[default]
    #[strum(to_string = "User")]
    User,
}

// impl core::fmt::Display for Roles {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         let rv = match self {
//             Roles::Admin => String::from("Admin"),
//             Roles::Anonymous => String::from("Anonymous"),
//             Roles::Moderator => String::from("Moderator"),
//             Roles::User => String::from("User"),
//             _ => String::from("Unknown"),
//         };
//
//         write!(f, "{}", rv)
//     }
// }

impl From<&str> for Roles {
    fn from(value: &str) -> Self {
        match value {
            "Admin" => Roles::Admin,
            _ => Roles::User,
        }
    }
}

impl From<String> for Roles {
    fn from(value: String) -> Self {
        match value.as_str() {
            "Admin" => Roles::Admin,
            _ => Roles::User,
        }
    }
}