use std::io::Write;

use diesel::{
    deserialize::{self, FromSql, FromSqlRow},
    expression::AsExpression,
    pg::{Pg, PgValue},
    serialize::{self, IsNull, Output, ToSql},
};
use rocket::serde::{Deserialize, Serialize};

/// **Word** Status.
#[derive(
    Debug,
    Default,
    PartialOrd,
    PartialEq,
    Eq,
    Ord,
    strum::Display,
    AsExpression,
    FromSqlRow,
    Deserialize,
    Serialize,
)]
#[serde(crate = "rocket::serde")]
#[diesel(sql_type = crate::schema::sql_types::WordStatus)]
pub enum WordStatus {
    /// Approved.
    #[strum(to_string = "APPROVED")]
    Approved,

    /// Deleted.
    #[strum(to_string = "DELETED")]
    Deleted,

    /// Draft.
    #[default]
    #[strum(to_string = "DRAFT")]
    Draft,

    /// Rejected.
    #[strum(to_string = "REJECTED")]
    Rejected,
}

// impl core::fmt::Display for WordStatus {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         let rv = match self {
//             WordStatus::Approved => String::from("Approved"),
//             WordStatus::Deleted => String::from("Deleted"),
//             WordStatus::Draft => String::from("Draft"),
//             WordStatus::NotApproved => String::from("NotApproved"),
//             _ => String::from("Unknown"),
//         };
//
//         write!(f, "{}", rv)
//     }
// }

impl ToSql<crate::schema::sql_types::WordStatus, Pg> for WordStatus {
    fn to_sql<'b>(&'b self, out: &mut Output<'b, '_, Pg>) -> serialize::Result {
        match *self {
            WordStatus::Approved => out.write_all(b"APPROVED")?,
            WordStatus::Deleted => out.write_all(b"DELETED")?,
            WordStatus::Draft => out.write_all(b"DRAFT")?,
            WordStatus::Rejected => out.write_all(b"REJECTED")?,
        }
        Ok(IsNull::No)
    }
}

impl FromSql<crate::schema::sql_types::WordStatus, Pg> for WordStatus {
    fn from_sql(bytes: PgValue) -> deserialize::Result<Self> {
        match bytes.as_bytes() {
            b"APPROVED" => Ok(WordStatus::Approved),
            b"DELETED" => Ok(WordStatus::Deleted),
            b"DRAFT" => Ok(WordStatus::Draft),
            b"REJECTED" => Ok(WordStatus::Rejected),
            _ => Err("Unrecognized enum variant".into()),
        }
    }
}
