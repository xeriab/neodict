use std::io::Write;

use diesel::{
    deserialize::{self, FromSql, FromSqlRow},
    expression::AsExpression,
    pg::{Pg, PgValue},
    serialize::{self, IsNull, Output, ToSql},
};
use rocket::serde::{Deserialize, Serialize};

/// **Role** Status.
#[derive(
    Debug,
    Default,
    PartialOrd,
    PartialEq,
    Eq,
    Ord,
    strum::Display,
    AsExpression,
    FromSqlRow,
    Deserialize,
    Serialize,
)]
#[diesel(sql_type = crate::schema::sql_types::RoleStatus)]
#[serde(crate = "rocket::serde")]
pub enum RoleStatus {
    /// Deleted.
    #[strum(to_string = "DELETED")]
    Deleted,

    /// Disabled.
    #[strum(to_string = "DISABLED")]
    Disabled,

    /// Draft.
    #[default]
    #[strum(to_string = "DRAFT")]
    Draft,

    /// Enabled.
    #[strum(to_string = "ENABLED")]
    Enabled,
}

// impl core::fmt::Display for RoleStatus {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         let rv = match self {
//             RoleStatus::Deleted => String::from("Deleted"),
//             RoleStatus::Disabled => String::from("Disabled"),
//             RoleStatus::Draft => String::from("Draft"),
//             RoleStatus::Enabled => String::from("Enabled"),
//             _ => String::from("Unknown"),
//         };
//
//         write!(f, "{}", rv)
//     }
// }

impl ToSql<crate::schema::sql_types::RoleStatus, Pg> for RoleStatus {
    fn to_sql<'b>(&'b self, out: &mut Output<'b, '_, Pg>) -> serialize::Result {
        match *self {
            RoleStatus::Deleted => out.write_all(b"DELETED")?,
            RoleStatus::Disabled => out.write_all(b"DISABLED")?,
            RoleStatus::Draft => out.write_all(b"DRAFT")?,
            RoleStatus::Enabled => out.write_all(b"ENABLED")?,
        }
        Ok(IsNull::No)
    }
}

impl FromSql<crate::schema::sql_types::RoleStatus, Pg> for RoleStatus {
    fn from_sql(bytes: PgValue) -> deserialize::Result<Self> {
        match bytes.as_bytes() {
            b"DELETED" => Ok(RoleStatus::Deleted),
            b"DISABLED" => Ok(RoleStatus::Disabled),
            b"DRAFT" => Ok(RoleStatus::Draft),
            b"ENABLED" => Ok(RoleStatus::Enabled),
            _ => Err("Unrecognized enum variant".into()),
        }
    }
}

