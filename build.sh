# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2024 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

#
# build.sh
#

set -e

#region Common
#

#
#endregion Common

#region Functions
#

# Main
#
Main() {
  return 0
}

#
#endregion Functions

#region Program
#

Main "$@"
exit 0

#
#endregion Program

# vim: set ts=2 sw=2 tw=80 noet :
