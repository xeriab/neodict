#!/usr/bin/env python
#
# -*- tab-width: 4; encoding: utf-8; mode: toml; -*-
#
# Copyright (c) 2024 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

from os import environ
from sys import argv
from typing import List as ListType, Dict as DictType, Set as SetType, Union as UnionType, \
    Tuple as TupleType, Any as AnyType

import requests

#
# Types
#

Any = UnionType[AnyType | None]
List = UnionType[list | ListType | None]
Dict = UnionType[dict | DictType | None]
Str = UnionType[str | None]
Tuple = UnionType[tuple | TupleType | None]
Set = UnionType[set | SetType | None]
Int = UnionType[int | None]
Float = UnionType[float | None]
Bool = UnionType[bool | None]

# DictList = UnionType[ListType[Dict] | None]
DictList = ListType[Dict] | ListType[dict] | list[Dict] | list[dict] | None
StrList = UnionType[ListType[Str] | None]
StrSet = UnionType[SetType[Str] | None]
Args = UnionType[Tuple | List | TupleType[Str] | TupleType[Any] | ListType[Str], ListType[Any]]

#
# Constants
#

GITHUB_TOKEN: Str = environ.get("GITHUB_TOKEN")
GITHUB_API_URL: Str = "https://api.github.com"
GITHUB_URL: Str = "https://github.com"

GITLAB_TOKEN: Str = environ.get("GITLAB_TOKEN")
GITLAB_API_URL: Str = "https://api.gitlab.com"
GITLAB_URL: Str = "https://gitlab.com"

REPO_NAME: Str = "xeriab/neodict"
MILESTONE: Str = "0.1"

# Number of items per page, adjust as needed.
MAX_PAGE_ITEMS: Int = 32


#
# Classes
#

class Tool(object):
    r"""Tool"""

    _url: Str = None
    _api: Str = None
    _token: Str = None
    _repo: Str = None

    api_url: Str = None
    web_url: Str = None
    token: Str = None
    repo: Str = None

    headers: Dict = {}

    def __init__(self, url: Str = None, api: Str = None, repo: Str = None):
        self._url = url
        self._api = api
        self._repo = repo

    def set_api_url(self, api_url: Str = None):
        self.api_url = api_url
        return self

    def get_api_url(self) -> Str:
        return self.api_url

    def set_web_url(self, web_url: Str = None):
        self.web_url = web_url
        return self

    def get_web_url(self) -> Str:
        return self.web_url

    def set_token(self, token: Str = None):
        self.token = token
        return self

    def get_token(self) -> Str:
        return self.token

    def set_repo(self, repo: Str = None):
        self.repo = repo
        return self

    def get_repo(self) -> Str:
        return self.repo

    def set_headers(self, headers: Dict = None):
        self.headers = headers
        return self

    def get_headers(self) -> Dict:
        return self.headers

    def add_header(self, key: Str = None, value: Any = None):
        if key and value:
            self.headers[key] = value
        return self

    def __dir__(self):
        return "<Tool>"

    def __repr__(self):
        return "<Tool>"


#
# Functions
#

#
# def _api_url(repo: Str, github: Bool = True) -> Str:
#     if github:
#         return f"{GITHUB_API_URL}/repos/{repo}/pulls"
#     return f"{GITLAB_API_URL}/repos/{repo}/pulls"
#

def _url(repo: Str, api: Bool = None, github: Bool = None, **kwargs: Dict) -> Str:
    r"""Returns the ``Git`` repository ``URL``.

    :param repo: ``Git`` repository.
    :param api: *(optional)* Returns :code:`API` ``URL``.
    :param github: *(optional)* Is it a GitHub ``URL``.
    :param \*\*kwargs: Optional arguments that ``_url`` takes.
    :return: :class:`str` *URL* string.
    :rtype: str | None
    """

    is_api: Bool = False
    is_github: Bool = False

    for key, value in kwargs.items():
        if key == "api" and value is True:
            is_api = True
        elif key == "github" and value is True:
            is_github = True

    if api and api is True:
        is_api = True
    elif github and github is True:
        is_github = True

    if is_github:
        if is_api:
            return f"{GITHUB_API_URL}/repos/{repo}"
        return f"{GITHUB_URL}/{repo}"

    if is_api:
        return f"{GITLAB_API_URL}/repos/{repo}"

    return f"{GITLAB_URL}/{repo}"


def _prs_url(repo: Str, api: Bool = None, github: Bool = None) -> Str:
    r"""Returns the ``Git`` pull/merge requests ``URL``.

    :param repo: ``Git`` repository.
    :param api: *(optional)* Returns :code:`API` ``URL``.
    :param github: *(optional)* Is it a GitHub ``URL``.
    :return: :class:`str` *URL* string.
    :rtype: str | None
    """

    is_api: Bool = False
    is_github: Bool = False

    if api and api is True:
        is_api = True
    elif github and github is True:
        is_github = True

    result: Str = _url(repo, api=is_api, github=is_github)

    if github:
        return f"{result}/pull"

    return f"{result}/pull"


def get_merged_prs(repo: Str, milestone: Str, token: Str) -> DictList:
    url: Str = f"{_prs_url(repo, api=True, github=True)}/pulls"
    params: Dict = {"state": "closed", "per_page": MAX_PAGE_ITEMS, "page": 1}
    # noinspection PyUnusedLocal
    headers: Dict = {"Authorization": f"token {token}"}
    rv: DictList = []
    page: Int = 1
    response: requests.Response | None = None
    prs: DictList = None

    while True:
        params["page"] = page

        try:
            # response = requests.get(url, params=params, headers=headers)
            response = requests.get(url, params=params)
            prs = response.json()
        except requests.HTTPError:
            response.raise_for_status()

        print(prs)

        # No more pages
        if not prs:
            break

        rv.extend([pr for pr in prs if
                   pr["merged_at"] and (pr["milestone"] or {}).get("title", "") == milestone])

        page += 1

    return rv


def categorize_prs(prs: DictList) -> Dict:
    result: Dict = {
        "addition": [],
        "change": [],
        "fix": [],
    }

    for pr in prs:
        labels: StrList = [label["name"] for label in pr["labels"]]

        if "addition" in labels or "feature" in labels:
            result["addition"].append(pr)
        elif "fix" in labels or "bug" in labels:
            result["fix"].append(pr)
        elif "change" in labels or "improvement" in labels:
            result["change"].append(pr)

    return result


def get_authors(prs: DictList) -> StrList:
    result: StrSet = set()

    for pr in prs:
        result.add(pr["user"]["login"])

    # noinspection PyTypeChecker
    return sorted(result, key=str.casefold)


def main(*args: Args, **kwargs: Dict) -> None:
    print("  args: ", args)
    print("kwargs: ", kwargs)

    prs: DictList = get_merged_prs(REPO_NAME, MILESTONE, GITHUB_TOKEN)

    categorized_prs: Dict = categorize_prs(prs)

    for category, items in categorized_prs.items():
        print(f"### {category.capitalize()}")

        for pr in items:
            title: Str = pr["title"]
            num: Int = pr["number"]
            print(f"- {title}. [#{num}]({GITHUB_URL}/{REPO_NAME}/pull/{num})")

        print("")

    print("")

    authors: StrList = get_authors(prs)

    print("Many thanks to...")

    for each in authors:
        print(f"- @{each}")


#
# Program
#

if __name__ == "__main__":
    main(argv[1:])

# vim: set ts=4 sw=4 tw=80 noet :
